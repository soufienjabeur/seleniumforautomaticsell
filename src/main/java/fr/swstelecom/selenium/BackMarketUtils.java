package fr.swstelecom.selenium;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import fr.swstelecom.selenium.model.Country;
import fr.swstelecom.selenium.model.PriceToApply;
import fr.swstelecom.selenium.model.Product;
import fr.swstelecom.selenium.site.ApplyBackMarket;
import fr.swstelecom.selenium.site.BackMarKetApi;

public class BackMarketUtils {

	WebDriver driver = new ChromeDriver();
	
//	public static void main(String[] args) throws GeneralSecurityException, IOException {
//		System.setProperty("webdriver.chrome.driver", "C:\\scom\\selenium\\src\\main\\resources\\chromedriver.exe");
//		BackMarketUtils backMarketUtils = new BackMarketUtils();
//		backMarketUtils.authenticate("ihsenbenamor@yahoo.fr", "NewSpaceTelecom!");
//		backMarketUtils.getProducts(1, Country.France);
//    }
	
	public String getProductsURL(int pageNumber, Country country){
		String url = "";
		switch (country) {
		case France:
			url ="https://www.backmarket.fr/bo_merchant/listings?page="+pageNumber+"&lang=fr-fr";
			break;
		case Belgique:
			url ="https://www.backmarket.fr/bo_merchant/listings?page="+pageNumber+"&lang=fr-be";
			break;
		case Espagne:
			url ="https://www.backmarket.fr/bo_merchant/listings?page="+pageNumber+"&lang=es-es";
			break;
		case Allemegne:
			url ="https://www.backmarket.fr/bo_merchant/listings?page="+pageNumber+"&lang=de-de";
			break;
		default:
			break;
		}
		
		return url;

	}
	
	
	/**
	 * aller � la page pageNumber  pour le country et r�cuperer tous les produits en vente  
	 * @param pageNumber
	 * @param country
	 * @return
	 */
	public List<Product> readProducts(String url, Country country) {
		List<Product> result = new ArrayList<Product>();
		driver.get(url);
		BackMarKetApi backMarKetApi = new BackMarKetApi();
		Product product = null;
		//backmarket table begin from 3thrd line
		int i = 3;
		do {
			product = backMarKetApi.readProductLine(driver, i, country);
			i++;
			if(product != null) {
				result.add(product);
			}
		}while(product != null);
		
		return result;
	}
	
	/**
	 * Aller � la page produit et appliquier les prix par pay
	 * @param product
	 * @param priceToApply
	 * @return
	 */
	public List<Product> applyPrices(List<PriceToApply> pricesToApply, String url, Country country) {
		List<Product> result = new ArrayList<Product>();
		driver.get(url);
		BackMarKetApi backMarKetApi = new BackMarKetApi();
		Product product = null;
		//backmarket table begin from 3thrd line
		int i = 3;
		do {
			product = backMarKetApi.readProductLine(driver, i, country);
			i++;
			if(product != null) {
				result.add(product);
			}
		}while(product != null);
		
		return result;
	}
	
	public void authenticate(String userName, String password) {
		// Find the text input element by its name
		driver.get("https://www.backmarket.fr/bo_merchant/");
        WebElement wusername = driver.findElement(By.id("id_username"));
        WebElement wpassword = driver.findElement(By.id("id_password"));

        // Enter something to search for
        wusername.sendKeys(userName);
        wpassword.sendKeys(password);
        wpassword.sendKeys(Keys.RETURN);
	}


	public String getAnnonceProductsURL(String sku, Country country) {
		String url = "";
		switch (country) {
		case France:
			url ="https://www.backmarket.fr/bo_merchant/listings?sku="+sku+"&lang=fr-fr";
			break;
		case Belgique:
			url ="https://www.backmarket.fr/bo_merchant/listings?sku="+sku+"&lang=fr-be";
			break;
		case Espagne:
			url ="https://www.backmarket.fr/bo_merchant/listings?sku="+sku+"&lang=es-es";
			break;
		case Allemegne:
			url ="https://www.backmarket.fr/bo_merchant/listings?sku="+sku+"&lang=de-de";
			break;
		default:
			break;
		}
		
		return url;
	}


	public void applyprice(String url, double priceToApply) {
		ApplyBackMarket applyBackMarket = new ApplyBackMarket();
		applyBackMarket.apply(driver, url, priceToApply);
		
	}
	
}
