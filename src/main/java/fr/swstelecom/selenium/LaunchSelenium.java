package fr.swstelecom.selenium;


import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import fr.swstelecom.selenium.SheetsApi.SpreadSheetsElements;
import fr.swstelecom.selenium.model.Country;
import fr.swstelecom.selenium.model.PriceCountry;
import fr.swstelecom.selenium.model.PriceToApply;
import fr.swstelecom.selenium.model.Product;
import fr.swstelecom.selenium.rules.BonPlanRule;
import fr.swstelecom.selenium.rules.MinMarginRule;
import fr.swstelecom.selenium.rules.PriceRulesExecutor;

public class LaunchSelenium {
	
	BackMarketUtils backMarketUtils;
	SpreadSheetsElements spreadSheetsElements;
	
	

	public LaunchSelenium() throws GeneralSecurityException, IOException {
		super();
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\LENOVO\\Downloads\\chromedriver_win32\\chromedriver.exe");
		backMarketUtils = new BackMarketUtils();
		spreadSheetsElements = new SpreadSheetsElements();
	}

	public static void main(String[] args) throws GeneralSecurityException, IOException {
		
		LaunchSelenium launchSelenium = new LaunchSelenium();
		launchSelenium.execute();
    }
	
	public void execute() {
		backMarketUtils.authenticate("ihsenbenamor@yahoo.fr", "NewSpaceTelecom!");
		List<Product> allProducts = getAllProducts();
		PriceRulesExecutor priceRulesExecutor = new MinMarginRule(spreadSheetsElements, allProducts);
//		BonPlanRule bonPlanRule = new BonPlanRule(allProducts, spreadSheetsElements);
//		priceRulesExecutor.setNextTaskRules(bonPlanRule);
		List<PriceToApply> pricesToApply = priceRulesExecutor.execute();
		applyPrices(pricesToApply);
		
//		List<PriceToApply> pricesToApply= getPricesToApply(allProducts);
		System.out.println(pricesToApply.size());
	}
	
	public List<Product> getAllProducts() {
		
		int numerOfProductsPerPage = 0;
		List<Product> allProducts = new ArrayList<Product>();
		
		for (Country country : Country.values()) {
			int i =1;
			List<Product> products = new ArrayList<Product>();
			do {
				 String url = backMarketUtils.getProductsURL(i, country);
				 products= backMarketUtils.readProducts(url, country);
				allProducts.addAll(products);
				if(i ==1) {
					numerOfProductsPerPage = products.size();
				}
				i++;
			}while(products.size() == numerOfProductsPerPage);
		}
		return mergeProducts(allProducts);
		
	}
	
	public void applyPrices(List<PriceToApply> pricesToApply) {
		
		int numerOfProductsPerPage = 0;

		for (PriceToApply priceToApply : pricesToApply) {
			for(PriceCountry priceCountry : priceToApply.getPricesToapply()) {
				String url = backMarketUtils.getAnnonceProductsURL(priceToApply.getSku(), priceCountry.getCountry());
				backMarketUtils.applyprice(url,priceCountry.getPriceToApply());
				
			}
		}

		
	}
	
//	public List<PriceToApply> getPricesToApply(List<Product> allProducts) {
//		List<PriceToApply> pricesToApply = new ArrayList<PriceToApply>();
//		for (Product product : allProducts) {
//			PriceToApply priceToApply = spreedSheetUtils.getPrice(product);
//			if(priceToApply!= null) {
//				pricesToApply.add(priceToApply);
//			}
//		}
//		return pricesToApply;
//	}
	
	private List<Product> mergeProducts(List<Product> allProducts) {
		Map<String, Product> toreturn = new HashMap<String, Product>();
		for(Product product : allProducts) {
			if(!toreturn.containsKey(product.getSku())){
				toreturn.put(product.getSku(), product);
			}else {
				toreturn.get(product.getSku()).getProductCountry().addAll(product.getProductCountry());
			}
		}
		return new ArrayList<Product>(toreturn.values());
	}
	
	
	private Map<Country, List<PriceToApply>> getcountryIndex(List<PriceToApply> pricesToApply){
		Map<Country, List<PriceToApply>> result = new HashMap<Country, List<PriceToApply>>();
		for (PriceToApply priceToApply : pricesToApply) {
//			Iterator<Pricecoun> iter = priceToApply.getPricesToapply().iterator();
			for (PriceCountry priceCountry : priceToApply.getPricesToapply()) {
				if(!result.containsKey(priceCountry.getCountry())) {
					result.put(priceCountry.getCountry(), new ArrayList<PriceToApply>());	
				}
				result.get(priceCountry.getCountry()).add(priceToApply);
//				
			}
		}
		return result;
	}
}
