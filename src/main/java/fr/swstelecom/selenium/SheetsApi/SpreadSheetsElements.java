package fr.swstelecom.selenium.SheetsApi;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.UpdateValuesResponse;
import com.google.api.services.sheets.v4.model.ValueRange;



public class SpreadSheetsElements {
	private static final String APPLICATION_NAME = "Automated price update";
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	private static final String TOKENS_DIRECTORY_PATH = "tokens";
	private final static String spreadsheetId = "1TpzOxk60mU9-F1OUXqLCXASB8ft9g1P3KYp7PKKBywQ";
	/**
	 * Global instance of the scopes required by this quickstart. If modifying these
	 * scopes, delete your previously saved tokens/ folder.
	 */
	private static final List<String> SCOPES = Collections.singletonList(SheetsScopes.SPREADSHEETS);
	private static final String CREDENTIALS_FILE_PATH = "/credentials.json";

	private Sheets service;

	public SpreadSheetsElements() throws GeneralSecurityException, IOException {
		super();
		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();

		service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
				.setApplicationName(APPLICATION_NAME).build();
	}

	private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
		// Load client secrets.
		InputStream in = SpreadSheetsElements.class.getResourceAsStream(CREDENTIALS_FILE_PATH);
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

		// Build flow and trigger user authorization request.
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY,
				clientSecrets, SCOPES)
						.setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
						.setAccessType("offline").build();
		LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
		return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
	}


	public static void main(String[] args) {
		SpreadSheetsElements spreadSheetsElements;
		try {
			spreadSheetsElements = new SpreadSheetsElements();
			//spreadSheetsElements.getSkuLine("IPSE32GDA");
			//System.out.println(spreadSheetsElements.getValueLineColumn("Y", 44));
			spreadSheetsElements.setCellValue("T", 44, 500);
		} catch (GeneralSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public int searchContentByColumn(String content, String columnRange) {
		int lineNumber = -1;
		try {
			ValueRange response = service.spreadsheets().values().get(spreadsheetId, columnRange).execute();

			List<List<Object>> values = response.getValues();
			if (values == null || values.isEmpty()) {

			} else {
				for (List row : values) {
					if (row.size() > 0) {
						if (content.equals(row.get(0))) {
							lineNumber = values.indexOf(row) + 1;
							break;
						}
					}
				}
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lineNumber;
	}
	
	/**
	 * 
	 * @param numeroLigne
	 * @return
	 * @throws IOException 
	 */
	
	public String getValueLineColumn(String column, int numeroLigne) throws IOException {
		String value = null;
		
			ValueRange response = service.spreadsheets().values().get(spreadsheetId, column +numeroLigne).execute();

			List<List<Object>> values = response.getValues();
			if (values == null || values.isEmpty()) {
			} else {
				for (List row : values) {
					value = row.get(0) +"";

				}
			}

		
		return value;
	}
	

	public boolean setCellValue(String column, int numeroLigne, double price) throws IOException {
		List<List<Object>> values = Arrays.asList(
		        Arrays.asList(
		        		price
		        )
		);
		ValueRange body = new ValueRange()
		        .setValues(values);
		UpdateValuesResponse result;

			result = service.spreadsheets().values().update(spreadsheetId, column+numeroLigne, body)
			        .setValueInputOption("RAW")
			        .execute();
		
		
		return true;
	}
	
}


