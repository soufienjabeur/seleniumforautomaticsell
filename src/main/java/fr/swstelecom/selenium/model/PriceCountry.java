package fr.swstelecom.selenium.model;

public class PriceCountry {
	Country country;
	double priceToApply;
	
	
	public PriceCountry() {
		super();
	}
	
	
	public Country getCountry() {
		return country;
	}


	public void setCountry(Country country) {
		this.country = country;
	}


	public double getPriceToApply() {
		return priceToApply;
	}
	public void setPriceToApply(double priceToApply) {
		this.priceToApply = priceToApply;
	}
	
	
	
}
