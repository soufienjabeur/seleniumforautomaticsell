package fr.swstelecom.selenium.model;

import java.util.ArrayList;
import java.util.List;

public class PriceToApply {
	String sku;
	int stock;
	List<PriceCountry> pricesToapply= new ArrayList<PriceCountry>();


	
	public PriceToApply() {
		super();
	}



	public List<PriceCountry> getPricesToapply() {
		return pricesToapply;
	}



	public void setPricesToapply(List<PriceCountry> pricesToapply) {
		this.pricesToapply = pricesToapply;
	}



	public String getSku() {
		return sku;
	}



	public void setSku(String sku) {
		this.sku = sku;
	}



	public int getStock() {
		return stock;
	}



	public void setStock(int stock) {
		this.stock = stock;
	}
	
	
	

}
