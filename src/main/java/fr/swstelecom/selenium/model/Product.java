package fr.swstelecom.selenium.model;

import java.util.ArrayList;
import java.util.List;

public class Product {
	
	List<ProductCountry> productCountry = new ArrayList<ProductCountry>();
	String sku;
	int stock;
	
	public Product() {
		super();
	}
	
	
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}


	public List<ProductCountry> getProductCountry() {
		return productCountry;
	}


	public void setProductCountry(List<ProductCountry> productCountry) {
		this.productCountry = productCountry;
	}
	
	
	
	
}
