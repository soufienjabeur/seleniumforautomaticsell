package fr.swstelecom.selenium.model;

public class ProductCountry {
	double pv;
	double pvpb;
	Country country;
	boolean enVente;
	
	public ProductCountry() {
		super();
	}
	public double getPv() {
		return pv;
	}
	public void setPv(double pv) {
		this.pv = pv;
	}
	public double getPvpb() {
		return pvpb;
	}
	public void setPvpb(double pvpb) {
		this.pvpb = pvpb;
	}
	public Country getCountry() {
		return country;
	}
	public void setCountry(Country country) {
		this.country = country;
	}
	public boolean isEnVente() {
		return enVente;
	}
	public void setEnVente(boolean enVente) {
		this.enVente = enVente;
	}
	
	
	
	
}
