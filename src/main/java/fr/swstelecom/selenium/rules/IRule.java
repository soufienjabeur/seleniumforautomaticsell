package fr.swstelecom.selenium.rules;

import java.util.List;

import fr.swstelecom.selenium.model.PriceToApply;

public interface IRule {
	public List<PriceToApply> checkRulePricess();
	public void setPricesToApply(List<PriceToApply> pricesToApply);

}
