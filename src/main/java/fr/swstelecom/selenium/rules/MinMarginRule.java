package fr.swstelecom.selenium.rules;

import java.util.ArrayList;
import java.util.List;

import fr.swstelecom.selenium.SheetsApi.SpreadSheetsElements;
import fr.swstelecom.selenium.model.PriceCountry;
import fr.swstelecom.selenium.model.PriceToApply;
import fr.swstelecom.selenium.model.Product;
import fr.swstelecom.selenium.model.ProductCountry;

public class MinMarginRule extends PriceRulesExecutor{
	
	SpreadSheetsElements spreadSheetsElements;
	List<Product> products;
	

	

	public MinMarginRule(SpreadSheetsElements spreadSheetsElements, List<Product> products) {
		super();
		this.spreadSheetsElements = spreadSheetsElements;
		this.products = products;
	}

	@Override
	public List<PriceToApply> checkRulePricess() {

		return getPricesToApply(products);
	}

	public List<PriceToApply> getPricesToApply(List<Product> allProducts) {
		List<PriceToApply> pricesToApply = new ArrayList<PriceToApply>();
		for (Product product : allProducts) {
			PriceToApply priceToApply = getPrice(product);
			if(priceToApply!= null) {
				pricesToApply.add(priceToApply);
			}
		}
		return pricesToApply;
	}
	
	/**
	 * se reposer su le fichier excel pour retourner les prix modifiables pour
	 * chaque pay
	 * 
	 * @param product
	 * @return
	 */
	public PriceToApply getPrice(Product product) {
		PriceToApply priceToApply = new PriceToApply();
		priceToApply.setSku(product.getSku());
		priceToApply.setStock(product.getStock());
		List<ProductCountry> productCountrys = product.getProductCountry();
		for (ProductCountry productCountry : productCountrys) {
			if (productCountry.isEnVente()) {
				// TODO when product is already open to sell
			} else {
				boolean isMarginRespected = checkIfMarginRespected(product.getSku(), productCountry);
				if (isMarginRespected) {
					PriceCountry priceCountry = new PriceCountry();
					priceCountry.setPriceToApply(productCountry.getPvpb());
					priceCountry.setCountry(productCountry.getCountry());
					priceToApply.getPricesToapply().add(priceCountry);
				}
			}
		}
		if(priceToApply.getPricesToapply().size()> 0) {
			return priceToApply;	
		}else {
			return null;
		}
		
	}
	
	
	private boolean checkIfMarginRespected(String sku, ProductCountry productCountry) {
		try {
			// trouver le num�ro de la ligne NL �quivalente � notre sku
			int NL = spreadSheetsElements.searchContentByColumn(sku,  "X1:X");
			// read min margin from sheets column Y
			String minMargin = spreadSheetsElements.getValueLineColumn("Y", NL);

			// apply price TNL and read margin value from UNL
			spreadSheetsElements.setCellValue("T", NL, productCountry.getPvpb());

			// read margin value
			String newMargin = spreadSheetsElements.getValueLineColumn("U", NL).replaceAll("%", "");

			// Check if margin value is less tan min margin and return true or false
			if (Integer.parseInt(newMargin) >= Integer.parseInt(minMargin)) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			System.out.println("SKU ISSUE in SHEETS "+ sku);
			return false;
		}
	}
	
}
