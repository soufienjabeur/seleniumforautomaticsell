package fr.swstelecom.selenium.rules;

import java.util.List;

import fr.swstelecom.selenium.model.PriceToApply;

public abstract class PriceRulesExecutor implements IRule{
	private IRule nextTaskRules;

	@Override
	public abstract List<PriceToApply> checkRulePricess();
	
	public List<PriceToApply> execute() {
		List<PriceToApply> result = checkRulePricess();
		if(result!= null && result.size()>0 && nextTaskRules!= null) {
			nextTaskRules.setPricesToApply(result);
			result = nextTaskRules.checkRulePricess();
		}
		return result;
	}

	public void setNextTaskRules(IRule nextTaskRules) {
		this.nextTaskRules = nextTaskRules;
	}
	
	public void setPricesToApply(List<PriceToApply> pricesToApply) {
	}

}
