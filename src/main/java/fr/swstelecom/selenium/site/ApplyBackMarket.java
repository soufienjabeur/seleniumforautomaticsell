package fr.swstelecom.selenium.site;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ApplyBackMarket {

	
	private final String X_INPUT = "//tr[3]/td[5]/input";
	
	public void apply(WebDriver driver, String url, double price) {
		driver.get(url);
		WebElement input = driver.findElement(By.xpath(X_INPUT));
		input.sendKeys(""+price);
		input.sendKeys(Keys.RETURN);
	}
	


}
