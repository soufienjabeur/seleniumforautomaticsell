package fr.swstelecom.selenium.site;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import fr.swstelecom.selenium.model.Country;
import fr.swstelecom.selenium.model.Product;
import fr.swstelecom.selenium.model.ProductCountry;

public class BackMarKetApi {
	private final String X_PREFIX = "//tr";
	private final String X_SKU = "/td[3]/a[1]";
	private final String X_PV = "/td[5]/span[2]";
	private final String X_PVPB = "/td[5]/span[3]";
	private final String X_STOCK = "/td[4]";

	public Product readProductLine(WebDriver driver, int line, Country country) {
		String linePrefix = X_PREFIX + "[" + line + "]";
		String stock = null;
		try {
			stock = driver.findElement(By.xpath(linePrefix + X_STOCK)).getText();
			if (Integer.parseInt(stock) == 0) {
				//no more products to explore in the table
				return null;
			}
		} catch (Exception e) {
			//end of table
			return null;
		}
		String sku = driver.findElement(By.xpath(linePrefix + X_SKU)).getText().replaceAll("SKU: ", "");
		String pv = driver.findElement(By.xpath(linePrefix + X_PV)).getText().replaceAll(",", ".");
		String pvpb = null;
		try {
			pvpb = driver.findElement(By.xpath(linePrefix + X_PVPB)).getText().replaceAll(",", ".");
		} catch (Exception e) {

		}

		ProductCountry productCountry = new ProductCountry();
		productCountry.setCountry(country);
		productCountry.setPv(Double.parseDouble(pv));
		if (pvpb == null) {
			productCountry.setEnVente(true);
		} else {
			productCountry.setPvpb(Double.parseDouble(pvpb));
			productCountry.setEnVente(false);
		}
		Product product = new Product();
		product.setSku(sku);
		product.setStock(Integer.parseInt(stock));
		product.getProductCountry().add(productCountry);

		return product;
	}
}
